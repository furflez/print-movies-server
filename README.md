
# Servidor
O servidor foi feito utilizando Node.js, Express e MongoDB para o banco de dados.

## Pré requisitos para o funcionamento

Node.js - https://nodejs.org. a versão deve ser maior que 8.x

Npm - Node.js package manager, já vem com o Node.js. versão deve ser maior que 5.x 

MongoDB -  https://www.mongodb.com, é importante ja ter um servidor mongo rodando da forma padrão.  manual de instalação: (https://docs.mongodb.com/manual/installation/)

## instalação

Após fazer o download dos arquivos, entrar na pasta e realizar o seguinte comando pelo cmd ou terminal

    npm install

este irá configurar e instalar todos os componentes utilizados no desenvolvimento.

## Executando
Para executar o cliente, realizar o seguinte comando pelo cmd ou terminal

    npm start
Ao iniciar, o servidor irá esperar por conexões em [http://127.0.0.1:3000](http://127.0.0.1:3000) por isso é importante que tanto o cliente quanto o servidor sejam executados no mesmo computador.

Na primeira execução será criado um usuário de teste para acessar o sistema:

    {
	    "fullName" : "Gerente do Cinemal",
	    "email" : "gerente@cinema.com",
	    "password" : "123456",
	    "role" : "gerente",
    }

utilizar o e-mail **gerente@cinema.com** e senha **123456** para efetuar o login
