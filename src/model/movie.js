const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MoviesSchema = Schema({
    image: String,
    title: String,
    description: String,
    duration: Number,
    animationType: String,
    audioType: String,
    sessions: [{ type: Schema.Types.ObjectId, ref: 'sessions' }],
});

module.exports = mongoose.model('movies', MoviesSchema);