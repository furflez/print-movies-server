const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoomsSchema = Schema({
    name: String,
    chairs: [String],
    lines: Number,
    columns: Number,
    sessions: [{ type: Schema.Types.ObjectId, ref: 'sessions' }],
});

module.exports = mongoose.model('rooms', RoomsSchema);