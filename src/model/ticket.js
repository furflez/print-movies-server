const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TicketsSchema = Schema({
    user: { type: Schema.Types.ObjectId, ref: 'users' },
    chairs: [String],
    snacks: [{ type: Schema.Types.ObjectId, ref: 'snacks' }],
    approved: Boolean,
    session: { type: Schema.Types.ObjectId, ref: 'sessions' },
});

module.exports = mongoose.model('tickets', TicketsSchema);