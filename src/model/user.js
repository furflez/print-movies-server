const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = Schema({
    fullName: String,
    email: String,
    password: String,
    role: String
});

module.exports = mongoose.model('users', UserSchema);