const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SessionsSchema = Schema({
    date: Date,
    startDate: Date,
    endDate: Date,
    room: { type: Schema.Types.ObjectId, ref: 'rooms' },
    movie: { type: Schema.Types.ObjectId, ref: 'movies' },

});

module.exports = mongoose.model('sessions', SessionsSchema);