const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SnacksSchema = Schema({
    image: String,
    name: String,
    price: Number
});

module.exports = mongoose.model('snacks', SnacksSchema);