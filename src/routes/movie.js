const express = require('express');
const router = express.Router({ mergeParams: true });
const Movies = require('../model/movie');

router.get('/', async(req, res) => {
    const movies = await Movies.find();
    res.send(movies);

});

router.get('/:id', async(req, res) => {
    const movies = await Movies.findOne({ _id: req.params.id });
    res.send(movies);
});

router.post('/', async(req, res, next) => {
    try {
        const movie = new Movies(req.body);
        await movie.save();

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar o filme', data: {} });
    }

});

router.put('/', async(req, res, next) => {

    try {
        await Movies.updateOne({ _id: req.body._id }, { $set: req.body });
        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao alterar o filme', data: {} });
    }

});

router.delete('/:id', async(req, res, next) => {
    try {
        await Movies.deleteOne({ _id: req.params.id });

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error);
        res.status(500);
        res.send({ success: false, message: 'falha ao remover o filme', data: {} });
    }
});

module.exports = router;