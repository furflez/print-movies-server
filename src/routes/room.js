const express = require('express');
const router = express.Router({ mergeParams: true });
const Rooms = require('../model/room');

router.get('/', async(req, res) => {
    const rooms = await Rooms.find();
    res.send(rooms);

});

router.get('/:id', async(req, res) => {
    const rooms = await Rooms.findOne({ _id: req.params.id });
    res.send(rooms);
});

router.post('/', async(req, res, next) => {
    try {
        const room = new Rooms(req.body);
        await room.save();

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar a sala', data: {} });
    }

});

router.put('/', async(req, res, next) => {

    try {
        await Rooms.updateOne({ _id: req.body._id }, { $set: req.body });
        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao alterar a sala', data: {} });
    }

});

router.delete('/:id', async(req, res, next) => {
    try {
        await Rooms.deleteOne({ _id: req.params.id });

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error);
        res.status(500);
        res.send({ success: false, message: 'falha ao remover a sala', data: {} });
    }
});

module.exports = router;