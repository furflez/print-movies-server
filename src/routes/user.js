const express = require('express');
const router = express.Router({ mergeParams: true });
const Users = require('../model/user');

router.get('/', async(req, res) => {
    const users = await Users.find();
    res.send(users);

});

router.get('/:id', async(req, res) => {
    const users = await Users.findOne({ _id: req.params.id });
    res.send(users);
});

router.post('/', async(req, res, next) => {
    try {
        const user = new Users(req.body);
        await user.save();

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.log(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar', data: {} });
    }

});

router.put('/', async(req, res, next) => {

    try {
        await Users.updateOne({ _id: req.body._id }, { $set: req.body });
        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.log(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar', data: {} });
    }

});

router.delete('/:id', async(req, res, next) => {
    try {
        await Users.deleteOne({ _id: req.params.id });

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error);
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar', data: {} });
    }
});

module.exports = router;