const express = require('express');
const router = express.Router({ mergeParams: true });
const User = require('../model/user');

const tokenSecret = 'fjke!ivvx9-32$&14kc'

var jwt = require('jsonwebtoken');


router.post('/login', async(req, res, next) => {

    let user = {};
    try {
        user = await User.findOne({ email: req.body.email, password: req.body.password }).select('-password');
    } catch (_error) {
        user = null;
    }
    console.log(await User.find({}))
    if (user) {

        var token = jwt.sign({ _id: user._id, email: user.email, name: user.fullName, picture: user.picture, role: user.role }, tokenSecret);
        res.status(200);
        res.send({ token: token });
    } else {
        res.status(401);
        res.send({})
    }
});

router.post('/register', async(req, res, next) => {
    try {
        req.body.role = 'cliente';
        const user = new User(req.body);
        await user.save();

        res.status(200);
        res.send({});
    } catch (error) {
        res.status(500);
        res.send({});
    }

});

router.use('/user', require('./user'));
router.use('/room', require('./room'));
router.use('/movie', require('./movie'));
router.use('/session', require('./session'));
router.use('/snack', require('./snack'));
router.use('/ticket', require('./ticket'));

module.exports = router;