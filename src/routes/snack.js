const express = require('express');
const router = express.Router({ mergeParams: true });
const Snacks = require('../model/snack');

router.get('/', async(req, res) => {
    const snacks = await Snacks.find();
    res.send(snacks);

});

router.get('/:id', async(req, res) => {
    const snacks = await Snacks.findOne({ _id: req.params.id });
    res.send(snacks);
});

router.post('/', async(req, res, next) => {
    try {
        const snack = new Snacks(req.body);
        await snack.save();

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar o snack', data: {} });
    }

});

router.put('/', async(req, res, next) => {

    try {
        await Snacks.updateOne({ _id: req.body._id }, { $set: req.body });
        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao alterar o snack', data: {} });
    }

});

router.delete('/:id', async(req, res, next) => {
    try {
        await Snacks.deleteOne({ _id: req.params.id });

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error);
        res.status(500);
        res.send({ success: false, message: 'falha ao remover o snack', data: {} });
    }
});

module.exports = router;