const express = require('express');
const router = express.Router({ mergeParams: true });
const Sessions = require('../model/session');
var _ = require('lodash');

router.get('/', async(req, res) => {
    const sessions = await Sessions.find().populate('movie').populate('room');
    res.send(sessions);

});

router.get('/by-date/:date', async(req, res) => {


    const date = new Date(req.params.date);
    date.setHours(0);
    date.setMinutes(0);
    const datePlus = new Date(date.getTime());

    datePlus.setDate(datePlus.getDate() + 1);

    let sessions = await Sessions.find({ date: { "$gte": date, "$lt": datePlus } }).populate('movie').populate('room');

    sessions = _.groupBy(sessions, function(o) {
        return o.movie._id;
    });

    sessions = _.values(sessions);

    res.send(sessions);


});

router.get('/:id', async(req, res) => {
    const sessions = await Sessions.findOne({ _id: req.params.id }).populate('movie').populate('room');
    res.send(sessions);
});


router.post('/', async(req, res, next) => {
    try {
        const session = new Sessions(req.body);
        await session.save();

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar a sessão', data: {} });
    }

});

router.put('/', async(req, res, next) => {

    try {
        await Sessions.updateOne({ _id: req.body._id }, { $set: req.body });
        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao alterar a sessão', data: {} });
    }

});

router.delete('/:id', async(req, res, next) => {
    try {
        await Sessions.deleteOne({ _id: req.params.id });

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error);
        res.status(500);
        res.send({ success: false, message: 'falha ao remover a sessão', data: {} });
    }
});

module.exports = router;