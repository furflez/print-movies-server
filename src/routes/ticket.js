const express = require('express');
const router = express.Router({ mergeParams: true });
const Tickets = require('../model/ticket');

router.get('/', async(req, res) => {
    const tickets = await Tickets.find();
    res.send(tickets);

});
router.get('/session/:id', async(req, res) => {
    let tickets = await Tickets.find({ session: req.params.id });
    tickets = tickets || [];
    res.send(tickets);
});
router.get('/:id', async(req, res) => {
    let tickets = await Tickets.findOne({ _id: req.params.id });
    res.send(tickets);
});

router.post('/', async(req, res, next) => {
    try {
        let ticket = new Tickets(req.body);
        await ticket.save();

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao cadastrar o ticket', data: {} });
    }

});

router.put('/', async(req, res, next) => {

    try {
        await Tickets.updateOne({ _id: req.body._id }, { $set: req.body });
        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error)
        res.status(500);
        res.send({ success: false, message: 'falha ao alterar o ticket', data: {} });
    }

});

router.delete('/:id', async(req, res, next) => {
    try {
        await Tickets.deleteOne({ _id: req.params.id });

        res.status(200);
        res.send({ success: true, message: 'ok', data: {} });
    } catch (error) {
        console.error(error);
        res.status(500);
        res.send({ success: false, message: 'falha ao remover o ticket', data: {} });
    }
});

module.exports = router;