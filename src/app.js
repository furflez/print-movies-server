const path = require('path');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const Users = require('./model/user');

const bodyParser = require('body-parser');

const app = express();

// connection to db
mongoose.connect('mongodb://localhost/printMoviesDB')
    .then(db => console.log('db connected'))
    .catch(err => console.log(err));

// importing routes
const indexRoutes = require('./routes/index');

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// cors
app.use(cors());
app.options('*', cors());

// routes
app.use('/', indexRoutes);


app.listen(app.get('port'),async () => {
    console.log(`server on port ${app.get('port')}`);

    if(await Users.count() == 0){
        const user = new Users({
            fullName: "Gerente do Cinemal",
            email: "gerente@cinema.com",
            password: "123456",
            role: "gerente"
        });
        await user.save();
    }

});